package com.ibm.assignment;

import lombok.Data;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
class Employee {

  private @Id @GeneratedValue Long EmployeeID;
  private @NonNull String FirstName;
  private String MiddleName;
  private @NonNull String LastName;

  Employee() {}

  Employee(String firstName, String middleName, String lastName) {
    this.FirstName = firstName;
    this.MiddleName = middleName;
    this.LastName= lastName;
  }
}